#include <WiFiNINA.h>
#include <PubSubClient.h>
#include "arduino_secrets.h"
#include <FastLED.h>

#define COLUMNS 50
#define LINES 2
// #define LED_TYPE WS2812
#define LED_TYPE WS2812B
// #define LED_TYPE WS2852
#define COLOR_ORDER GRB // RGB
#define STRIP_PIN 5
// Parameters

char ssid[] = SECRET_SSID; // your network SSID (name)
char pass[] = SECRET_PASS; // your network password (use for WPA, or use as key for WEP)

enum State
{
  read,
  display
};
State dataState = read;

int wifi_status = WL_IDLE_STATUS;
WiFiClient wClient;
PubSubClient mqttClient;
const int NUM_LEDS = COLUMNS * LINES;
uint16_t colors[NUM_LEDS] = {};

// Objects
CRGB leds[NUM_LEDS];
uint8_t brightness = 10;

void setup()
{

  // Initialize serial and wait for port to open:
  Serial.begin(9600);

  while (!Serial)
    ;

  // attempt to connect to Wifi network:

  while (wifi_status != WL_CONNECTED)
  {

    Serial.print("Attempting to connect to network: ");

    Serial.println(ssid);

    // Connect to WPA/WPA2 network:

    wifi_status = WiFi.begin(ssid, pass);

    // wait 10 seconds for connection:

    delay(10000);
  }

  // you're connected now, so print out the data:

  Serial.println("You're connected to the network");

  Serial.println("----------------------------------------");

  printData();

  Serial.println("----------------------------------------");

  mqttClient.setClient(wClient);
  mqttClient.setServer(SECRET_MQTT_HOST, 1883);
  mqttClient.connect(SECRET_MQTT_ID, SECRET_MQTT_ID, SECRET_MQTT_PASS);
  mqttClient.setCallback(callback);

  while (!mqttClient.connected())
  {
    delay(1000);
  }
  Serial.println("You're connected to the MQTT Broker");

  Serial.println("----------------------------------------");

  mqttClient.subscribe("test");

  FastLED.addLeds<LED_TYPE, STRIP_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(brightness);
}

void loop()
{

  // check the network connection once every 10 seconds:
  if (dataState == display)
  {
    draw();
  }
  else
  {
    mqttClient.loop();
  }

  // printData();

  // Serial.println("----------------------------------------");
}

void draw()
{
  FastLED.setBrightness(brightness);
  for (size_t i = 0; i < NUM_LEDS; i++)
  {
    char buffer[40];
    sprintf(buffer, "Led[%d]: R(%d),G(%d),B(%d)", i,
            (colors[i] & 0xF00) * 0xFF / 0xF00,
            (colors[i] & 0xF0) * 0xFF / 0xF0,
            (colors[i] & 0xF) * 0xFF / 0xF);

    Serial.println(buffer);
    leds[i].setRGB(
        (colors[i] & 0xF00) * 0xFF / 0xF00,
        (colors[i] & 0xF0) * 0xFF / 0xF0,
        (colors[i] & 0xF) * 0xFF / 0xF);
  }
  FastLED.show();
  dataState = read;
}

void printData()
{

  Serial.println("Board Information:");

  // print your board's IP address:

  IPAddress ip = WiFi.localIP();

  Serial.print("IP Address: ");

  Serial.println(ip);

  Serial.println();

  Serial.println("Network Information:");

  Serial.print("SSID: ");

  Serial.println(WiFi.SSID());

  // print the received signal strength:

  long rssi = WiFi.RSSI();

  Serial.print("signal strength (RSSI):");

  Serial.println(rssi);
}

void callback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  brightness = payload[0];
  for (int i = 1; i < length - 1; i += 2)
  {
    Serial.print(payload[i]);
    Serial.print((char)' ');
    Serial.print(payload[i + 1]);
    Serial.print((char)' ');
    colors[i / 2] = payload[i] * 0x100 + payload[i + 1];
  }
  Serial.println();
  dataState = display;
}